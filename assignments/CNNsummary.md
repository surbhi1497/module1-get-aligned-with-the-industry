**Convolutional Neural Network**
A Convolutional Neural Network(CNN) is a class of deep neural networks, most commonly applied to analyzing visual imagery. They are also known as shift invariant or space invariant artificial neural networks, based on their shared-weights architecture and translation invariance characteristics.

*How does a CNN work?*
For CNN models to train and test, each input image will go through a series of convolution layers with filters (Kernals), Pooling, fully connected layers (FC) and apply Softmax function to classify an object with probabilistic values between 0 and 1. The below figure is a complete flow of CNN to process an input image and classifies the objects based on values.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/CNN.jpg


*Terminology :*

1. Kernals
A filter that is used to extract the features from the images. The kernel is a matrix that moves over the input data, performs the dot product with the sub-region of input data, and gets the output as the matrix of dot products.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/kernel.png

2. Stride
Stride is the number of pixels shifts over the input matrix. When the stride is 1 then we move the filters to 1 pixel at a time.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/stride.png

3. ReLU 
ReLU stands for Rectified Linear Unit for a non-linear operation. The output is ƒ(x) = max(0,x)
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/ReLU.jpg

4. Zero Padding
The process of symmetrically adding zeroes to the input matrix. It's a commonly used modification that allows the size of the input to be adjusted to our requirement.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/zer_padd.png

5. Convolutional Layer
The convolutional layer is the core building block of a CNN. The layer's parameters consist of a set of learnable filters (or kernels), which have a small receptive field, but extend through the full depth of the input volume.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/convlayer.jpg

6. Pooling Layer
Pooling layer is a new layer added after convolutional layer. Its function is to progressively reduce the spatial size of the representation to reduce the amount of parameters and computation in the network.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/pool_layer.png

7. Fully Connected Layer
Fully connected layers connect every neuron in one layer to every neuron in another layer.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/fully_conn_layer.jpg

8. Flattening
Flattening is converting the data into a 1-dimensional array for inputting it to the next layer. We flatten the output of the convolutional layers to create a single long feature vector. 
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/flattening.png


